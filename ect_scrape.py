import requests_cache
import requests
from bs4 import BeautifulSoup
import re
from datetime import date
import json

requests_cache.install_cache("cache/ect_cache")  # cache forever

geojson = {
    "type": "FeatureCollection",
    "features": [

    ]
}

# Gets cafés from the blank search page
# There is a sitemap which would be a lot better to use instead


def get_cafe_links():
    response = requests.get("https://europeancoffeetrip.com/?s=")

    html_content = response.text
    soup = BeautifulSoup(html_content, 'lxml')

    cafe_links = [a["href"] for a in soup.select(
        'a') if "https://europeancoffeetrip.com/cafe/" in a["href"]]

    return cafe_links


def get_feature(soup, link):
    print(link)

    # breadcrumbs contain city and country information
    breadcrumbs = soup.select_one(".cg-title-style")

    bc_texts = []
    for bc in breadcrumbs:
        if hasattr(bc, "text") and bc.text.strip():
            bc_texts.append(bc.text.strip())

    country = bc_texts[1]
    city = bc_texts[2]
    # name = bc_texts[3]

    # coordinates are encoded in Google Maps image URL
    image_src = soup.select_one("#cafe-map img")["src"]
    latitude, longitude = image_src.split(
        "%7C")[1].split("&key=")[0].split(",")

    print(latitude, longitude)

    # address and name are much simpler
    address = soup.select_one("div.cafe-address").text.strip()
    name = soup.select_one("h1.cafe-name").text.strip()

    return {
        "type": "Feature",
        "properties": {
            "name": name,
            "city": city,
            "country": country,
            "address": address,
            "source": "European Coffee Trip",
            "source_url": link,

            # for kepler
            "lat": float(latitude),
            "lng": float(longitude),

        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                float(longitude),
                float(latitude),
            ]
        }
    }


def get_cafe_data(cafe_links):

    count = len(cafe_links)

    c = 1
    for link in cafe_links:
        print(f"{c}/{count}")
        response = requests.get(link)
        html_content = response.text
        soup = BeautifulSoup(html_content, 'lxml')

        if soup.select_one(".error404-title-wrap"):
            print(f"{link} is 404")
            continue

        feature = get_feature(soup, link)

        geojson["features"].append(feature)
        c += 1


cafe_links = set(get_cafe_links())

get_cafe_data(list(cafe_links))

today = date.today().isoformat()

with open(f"ect_{today}.geojson", "w") as fp:
    json.dump(geojson, fp, indent=4)
