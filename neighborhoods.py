
import math
import json
import requests
import requests_cache
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import sys
from datetime import date


retries = Retry(total=10, backoff_factor=1,
                status_forcelist=[429, 500, 502, 503, 504])
http = requests.Session()
http.mount("https://", HTTPAdapter(max_retries=retries))

overpass_url = "http://overpass-api.de/api/interpreter"

requests_cache.install_cache("cache/overpass_cache")  # cache forever

RADIUS = 0.12  # 120m around cafés, a bit more than 1 minute walking distance


amenities = {
    "sustenance": [
        "bar",
        "biergarten",
        "cafe",
        "fast_food",
        "food_court",
        "ice_cream",
        "pub",
        "restaurant",
    ],

    "education": [
        "college",
        "driving_school",
        "kindergarten",
        "language_school",
        "library",
        "toy_library",
        "research_institute",
        "training",
        "music_school",
        "school",
        "traffic_park",
        "university",
    ],

    "transportation": [
        "bicycle_parking",
        "bicycle_repair_station",
        "bicycle_rental",
        "boat_rental",
        "boat_sharing",
        "bus_station",
        "car_rental",
        "car_sharing",
        "car_wash",
        "compressed_air",
        "vehicle_inspection",
        "charging_station",
        "driver_training",
        "ferry_terminal",
        "fuel",
        "grit_bin",
        "motorcycle_parking",
        "parking",
        "parking_entrance",
        "parking_space",
        "taxi",
    ],

    "financial": [
        "atm",
        "bank",
        "bureau_de_change",
    ],

    "healthcare": [
        "baby_hatch",
        "clinic",
        "dentist",
        "doctors",
        "hospital",
        "nursing_home",
        "pharmacy",
        "social_facility",
        "veterinary",
    ],

    "entertainment_arts_and_culture": [
        "arts_centre",
        "brothel",
        "casino",
        "cinema",
        "community_centre",
        "conference_centre",
        "events_venue",
        "exhibition_centre",
        "fountain",
        "gambling",
        "love_hotel",
        "music_venue",
        "nightclub",
        "planetarium",
        "public_bookcase",
        "social_centre",
        "stripclub",
        "studio",
        "swingerclub",
        "theatre",
    ],

    "public service": [
        "courthouse",
        "fire_station",
        "police",
        "post_box",
        "post_depot",
        "post_office",
        "prison",
        "ranger_station",
        "townhall",
    ],

    # "facilities": [
    #     "bbq",
    #     "bench",
    #     "dog_toilet",
    #     "dressing_room",
    #     "drinking_water",
    #     "give_box",
    #     "mailroom",
    #     "parcel_locker",
    #     "shelter",
    #     "shower",
    #     "telephone",
    #     "toilets",
    #     "water_point",
    #     "watering_place",
    # ],

    # "waste_management": [
    #     "sanitary_dump_station",
    #     "recycling",
    #     "waste_basket",
    #     "waste_disposal",
    #     "waste_transfer_station",
    # ],

    # "others": [
    #     "animal_boarding",
    #     "animal_breeding",
    #     "animal_shelter",
    #     "animal_training",
    #     "baking_oven",
    #     "childcare",
    #     "clock",
    #     "crematorium",
    #     "dive_centre",
    #     "funeral_hall",
    #     "grave_yard",
    #     "hunting_stand",
    #     "internet_cafe",
    #     "kitchen",
    #     "kneipp_water_cure",
    #     "lounger",
    #     "marketplace",
    #     "monastery",
    #     "photo_booth",
    #     "place_of_mourning",
    #     "place_of_worship",
    #     "public_bath",
    #     "public_building",
    #     "refugee_site",
    #     "vending_machine",
    #     "user defined",
    # ]
}


shops = {
    "food_beverages": [
        "alcohol",
        "bakery",
        "beverages",
        "brewing_supplies",
        "butcher",
        "cheese",
        "chocolate",
        "coffee",
        "confectionery",
        "convenience",
        "deli",
        "dairy",
        "farm",
        "frozen_food",
        "greengrocer",
        "health_food",
        "ice_cream",
        "pasta",
        "pastry",
        "seafood",
        "spices",
        "tea",
        "wine",
        "water",
    ],

    "general": [
        "department_store",
        "general",
        "kiosk",
        "mall",
        "supermarket",
        "wholesale",
    ],

    "clothing": [
        "baby_goods",
        "bag",
        "boutique",
        "clothes",
        "fabric",
        "fashion",
        "fashion_accessories",
        "jewelry",
        "leather",
        "sewing",
        "shoes",
        "tailor",
        "watches",
        "wool",
    ],

    "discount_charity": [
        "charity",
        "second_hand",
        "variety_store",
    ],

    "health_beauty": [
        "beauty",
        "chemist",
        "cosmetics",
        "erotic",
        "hairdresser",
        "hairdresser_supply",
        "hearing_aids",
        "herbalist",
        "massage",
        "medical_supply",
        "nutrition_supplements",
        "optician",
        "perfumery",
        "tattoo",
    ],

    "diy_household_garden": [
        "agrarian",
        "appliance",
        "bathroom_furnishing",
        "doityourself",
        "electrical",
        "energy",
        "fireplace",
        "florist",
        "garden_centre",
        "garden_furniture",
        "gas",
        "glaziery",
        "groundskeeping",
        "hardware",
        "houseware",
        "locksmith",
        "paint",
        "pottery",
        "security",
        "trade",
        "windows",
    ],

    "furtniture_interior": [
        "antiques",
        "bed",
        "candles",
        "carpet",
        "curtain",
        "doors",
        "flooring",
        "furniture",
        "household_linen",
        "interior_decoration",
        "kitchen",
        "lighting",
        "tiles",
        "window_blind",
    ],

    "electronics": [
        "computer",
        "electronics",
        "hifi",
        "mobile_phone",
        "radiotechnics",
        "telecommunication",
        "vacuum_cleaner",
    ],

    "outdoor_sport_vehicles": [
        "atv",
        "bicycle",
        "boat",
        "car",
        "car_repair",
        "motorcycle_repair",
        "car_parts",
        "caravan",
        "fuel",
        "fishing",
        "surf",
        "golf",
        "hunting",
        "jetski",
        "military_surplus",
        "motorcycle",
        "outdoor",
        "scuba_diving",
        "ski",
        "snowmobile",
        "sports",
        "swimming_pool",
        "trailer",
        "tyres",
    ],

    "art_music_hobbies": [
        "art",
        "camera",
        "collector",
        "craft",
        "frame",
        "games",
        "model",
        "music",
        "musical_instrument",
        "photo",
        "trophy",
        "video",
        "video_games",
    ],

    "stationary_gifts_books": [
        "anime",
        "books",
        "gift",
        "lottery",
        "newsagent",
        "stationery",
        "ticket",
    ],

    "other": [
        "bookmaker",
        "cannabis",
        "copyshop",
        "dry_cleaning",
        "e-cigarette",
        "funeral_directors",
        "insurance",
        "laundry",
        "money_lender",
        "outpost",
        "party",
        "pawnbroker",
        "pest_control",
        "pet",
        "pet_grooming",
        "pyrotechnics",
        "religion",
        "storage_rental",
        "tobacco",
        "toys",
        "travel_agency",
        "weapons",
        "vacant",
        "yes",
        "user defined",
    ],
}


# exit()


class BoundingBox(object):
    def __init__(self, *args, **kwargs):
        self.lat_min = None
        self.lon_min = None
        self.lat_max = None
        self.lon_max = None


"""  
https://stackoverflow.com/questions/1648917/given-a-latitude-and-longitude-and-distance-i-want-to-find-a-bounding-box
"""


def get_bounding_box(latitude_in_degrees, longitude_in_degrees, half_side_in_km):
    assert latitude_in_degrees >= -90.0 and latitude_in_degrees <= 90.0
    assert longitude_in_degrees >= -180.0 and longitude_in_degrees <= 180.0

    lat = math.radians(latitude_in_degrees)
    lon = math.radians(longitude_in_degrees)

    radius = 6371
    # Radius of the parallel at given latitude
    parallel_radius = radius*math.cos(lat)

    lat_min = lat - half_side_in_km/radius
    lat_max = lat + half_side_in_km/radius
    lon_min = lon - half_side_in_km/parallel_radius
    lon_max = lon + half_side_in_km/parallel_radius
    rad2deg = math.degrees

    box = BoundingBox()
    box.lat_min = rad2deg(lat_min)
    box.lon_min = rad2deg(lon_min)
    box.lat_max = rad2deg(lat_max)
    box.lon_max = rad2deg(lon_max)

    return (box)


"""
    type: amenity, shop
"""


def build_query(category, bbox, amenities, type):
    overpass_query = "[out:json];"
    overpass_query += "("

    # 51.477 is the latitude of the southern edge.
    # -0.001 is the longitude of the western edge.
    # 51.478 is the latitude of the norther edge.
    # 0.001 is the longitude of the eastern edge.
    bbox = ",".join(map(str,
                        [
                            bbox.lat_min,
                            bbox.lon_min,
                            bbox.lat_max,
                            bbox.lon_max
                        ]
                        ))
    for amenity in amenities[category]:
        overpass_query += f'    node["{type}" = "{amenity}"]({bbox});'
    overpass_query += ");"
    overpass_query += "out body;"

    return overpass_query


# overpass_query = """
# [out:json];
# (
#     node["amenity"="cafe"](53.2987342,-6.3870259,53.4105416,-6.1148829);
# );
# out count;
# """

def make_feature(element, category):
    feature = {
        "type": "Feature",
        "properties": {
            "name": element["tags"].get("name", "Unknown"),
            "amenity": element["tags"].get("amenity", False),
            "shop": element["tags"].get("shop", False),
            "category": category,

            # for kepler
            "lat": element["lat"],
            "lng": element["lon"],
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                    element["lon"],
                    element["lat"],
            ]
        }
    }
    return feature

# Add all elements from overpass


def add_all_elements(elements, category, type):
    for element in elements:

        if type == "amenity" and element["id"] not in elements_amenities:
            elements_amenities[element["id"]] = make_feature(element, category)

        if type == "shop" and element["id"] not in elements_shops:
            elements_shops[element["id"]] = make_feature(element, category)


# main geojson
geojson = {
    "type": "FeatureCollection",
    "features": [

    ]
}

# indexed by osm id
elements_amenities = {}
elements_shops = {}


# geojson for amenities and shops
geojson_amenities = {
    "type": "FeatureCollection",
    "features": [

    ]
}

geojson_shops = {
    "type": "FeatureCollection",
    "features": [

    ]
}

file = sys.argv[1] if len(sys.argv) > 1 else "ect_2023-08-22.geojson"

with open(file, "r") as fp:
    geojson = json.load(fp)

count = len(geojson["features"])

c = 0

for cafe in geojson["features"]:

    c += 1

    print(f" {c}/{count} " + cafe["properties"]["name"])

    bbox = get_bounding_box(
        cafe["geometry"]["coordinates"][1], cafe["geometry"]["coordinates"][0], RADIUS)

    for category in amenities:

        overpass_query = build_query(category, bbox, amenities, "amenity")
        response = requests.get(overpass_url,
                                params={'data': overpass_query}, timeout=5)
        data = response.json()

        count_elements = len(data["elements"])
        cafe["properties"][f"count_amenity_{category}"] = count_elements
        add_all_elements(data["elements"], category, "amenity")

    for category in shops:

        overpass_query = build_query(category, bbox, shops, "shop")
        response = requests.get(overpass_url,
                                params={'data': overpass_query}, timeout=5)
        data = response.json()

        count_elements = len(data["elements"])
        cafe["properties"][f"count_shop_{category}"] = count_elements
        add_all_elements(data["elements"], category, "shop")


today = date.today().isoformat()

for feature in elements_amenities.values():
    geojson_amenities["features"].append(feature)

for feature in elements_shops.values():
    geojson_shops["features"].append(feature)

# file for cafés
with open(f"specialty_neighborhoods_{today}.geojson", "w") as fp:
    json.dump(geojson, fp, indent=4)

# file for amenities in the neighborhood
with open(f"specialty_neighborhoods_amenities_{today}.geojson", "w") as fp:
    json.dump(geojson_amenities, fp, indent=4)

# file for shops in the neighborhood
with open(f"specialty_neighborhoods_shops_{today}.geojson", "w") as fp:
    json.dump(geojson_shops, fp, indent=4)


print(f"categories amenities: {len(amenities)}")
print(f"categories shops: {len(shops)}")
