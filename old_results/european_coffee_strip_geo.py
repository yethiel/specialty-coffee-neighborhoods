import requests
import re
import json
from datetime import date

print("Getting data...")
response = requests.get(f"https://europeancoffeetrip.com/?s=")
result = re.search(
    '(?<=<div class="cg-list">)([\S\s]*?)(?=</div>[\s]*<div class="clearboth">[\s]*</div>)', response.content.decode("utf-8")).group()
download_data = {}
cities = re.findall(r'((?<=<a href=")(.*)(?=">))', result)

failed_cities = []
cities_without_list = []


geojson = {
    "type": "FeatureCollection",
    "features": [

    ]
}


def get_feature(data_from_website):
    if not data_from_website["longitude"]:
        print("bad coords in", data_from_website["name"])
    return {
        "type": "Feature",
        "properties": {
            "address": data_from_website["address"] if "address" in data_from_website else "",
            "link": data_from_website["link"] if "link" in data_from_website else "",
            "featured_photo": data_from_website["featured_photo"] if "featured_photo" in data_from_website else "",
            "name": data_from_website["name"] if "name" in data_from_website else "",
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                float(data_from_website["longitude"]
                      ) if "longitude" in data_from_website else "",
                float(data_from_website["latitude"]
                      ) if "latitude" in data_from_website else "",
            ]
        }
    }


for city in cities:

    city = city[0]
    response = requests.get(city)

    # Cafes are stored in an array
    result = re.search(r"(?<=var cafes = )(.*?)(?=;)",
                       response.content.decode("utf-8"))

    if result:
        try:
            result_json = json.loads(result.group())
            download_data[city] = result_json
            for cafe in result_json:
                geojson["features"].append(get_feature(cafe))
        except:
            print(f"\n{city} failed")
            failed_cities.append(city)
    else:
        print(f"\n{city} has no cafe list")
        cities_without_list.append(city)

    print(".", end="")

print("\nDone downloading cities")

with open(f"coffee_{date.today()}.geojson", "w") as fp:
    json.dump(geojson, fp)

print("Failed:")
print(failed_cities)

print("No list:")
print(cities_without_list)
