import os
import json

# Combines all geojson files into one file called specialty.py

geojson = {
    "type": "FeatureCollection",
    "features": [

    ]
}

for fname in os.listdir('../geojson'):

    if "edited_" not in fname:
        continue

    with open(os.path.join("../geojson", fname), "r") as fp:
        gj = json.load(fp)

        for feature in gj["features"]:
            geojson["features"].append(feature)


# with open("ect_2023-06-01.geojson", "r") as fp:
#     gj = json.load(fp)

#     for feature in gj["features"]:
#         geojson["features"].append(feature)

print("count cafes:", len(geojson["features"]))

with open("thirdwavenearme.geojson", "w") as fp:
    json.dump(geojson, fp, indent=4)
