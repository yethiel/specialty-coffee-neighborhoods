# https://nominatim.openstreetmap.org/search?<params>

# q=<query>

# Free-form query string to search for. Free-form queries are processed first left-to-right and then right-to-left if that fails. So you may search for pilkington avenue, birmingham as well as for birmingham, pilkington avenue. Commas are optional, but improve performance by reducing the complexity of the search.

# amenity=<name and/or type of POI>
# street=<housenumber> <streetname>
# city=<city>
# county=<county>
# state=<state>
# country=<country>

# postalcode=<postalcode>

#
# format=[xml|json|jsonv2|geojson|geocodejson] (default jsonv2)


from dataclasses import dataclass


@dataclass
class Cafe:
    city: str
    category: str
    name: str
    address: str

    def validate(self):
        print(self)

        if not self.city:
            print("city missing")
            exit()

        if not self.category:
            print("category missing")
            exit()

        if not self.name:
            print("name missing")
            exit()

        if not self.address:
            print("address missing")
            exit()


cafes = []

# city, category, name, address
with open("italian_coffee_cafes.csv", "r") as f:
    lines = f.readlines()
    header = lines[0]
    entries = lines[1:]

    for entry in entries:
        entry = entry.split(",")
        cafes.append(Cafe(
            entry[0],  # city
            entry[1],  # category
            entry[2],  # name
            entry[3],  # address
        ))


# city, name, address, link
with open("italian_coffee_roasters.csv", "r") as f:
    lines = f.readlines()
    header = lines[0]
    entries = lines[1:]

    for entry in entries:
        entry = entry.split(",")
        cafes.append(Cafe(
            entry[0],  # city
            "Roaster",  # category
            entry[1],  # name
            entry[2],  # address
        ))

[cafe.validate() for cafe in cafes]
print("Done")
