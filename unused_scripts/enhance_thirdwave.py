import os
import json

# Adds city and country to geojson

if not os.path.isdir("geojson"):
    os.makedirs("geojson")


for f in os.listdir("geojson"):

    outfname = os.path.join("geojson", f"edited_{f}")

    if not os.path.isfile(os.path.join("geojson", f)):
        continue

    if "edited_" in f:
        continue

    country_ = f.split("_")[0]
    country = country_.title().replace('-', ' ')
    city_ = f.split("_")[1].split("__")[0]
    city = city_.title().replace('-', ' ')

    print(country)
    print(city)

    with open(f"geojson/{f}", "r") as fp:
        geojson = json.loads(fp.read())

        for feature in geojson["features"]:

            feature["properties"] = {
                "name": feature["properties"]["name"],
                "country": country,
                "city": city,
                "source": "Third Wave Near Me",
                "source_url": f"https://thirdwavenearme.com/{country_}/{city_}/"
            }
        with open(outfname, "w") as f3:
            f3.write(json.dumps(geojson, indent=4))
