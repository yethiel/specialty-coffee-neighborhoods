#!/bin/bash

# thirdwavenearme.com files are in kml and have to be converted
echo "Converting kml to geojson"
python alltogeo.py &&

# City and country information is missing from these files
echo "Adding country and city information to thirdwave near me"
python enhance_thirdwave.py

echo "Combining all files in geojson/"
python combine_geojson.py
