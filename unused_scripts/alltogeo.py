import os


# Converts all kml files in kml dir to geojson
for f in os.listdir("kml"):
    outname = f.replace(".kml", ".geojson")

    if not os.path.isfile(os.path.join("geojson", outname)):
        os.system(" ".join(
            ["togeojson",
             os.path.join("kml", f),
             ">",
             os.path.join("geojson", outname)
             ])
        )
