

import requests
from bs4 import BeautifulSoup
import re
import json
from datetime import date

print("Getting data...")
response = requests.get(f"http://thirdwavenearme.com/")

html_content = response.text
soup = BeautifulSoup(html_content, 'lxml')

links = ["http://thirdwavenearme.com" + a["href"]
         for a in soup.select("td.place a")]

print(links)


for link in links:
    response = requests.get(link)
    soup = BeautifulSoup(response.text, 'lxml')

    gmap = soup.select("a.map-link")[0]

    gmap_link = gmap["href"]

    mid = gmap_link.split("?mid=")[1]

    download_kml = f"https://www.google.com/maps/d/u/0/kml?mid={mid}&forcekml=1"

    response = requests.get(download_kml).text

    fname = link.split(".com/")[1].replace("/", "_") + f"_{mid}.kml"

    with open(fname, "w") as f:
        f.write(response)


exit()
failed_cities = []
cities_without_list = []


geojson = {
    "type": "FeatureCollection",
    "features": [

    ]
}


def get_feature(data_from_website):
    if not data_from_website["longitude"]:
        print("bad coords in", data_from_website["name"])
    return {
        "type": "Feature",
        "properties": {
            "address": data_from_website["address"] if "address" in data_from_website else "",
            "link": data_from_website["link"] if "link" in data_from_website else "",
            "featured_photo": data_from_website["featured_photo"] if "featured_photo" in data_from_website else "",
            "name": data_from_website["name"] if "name" in data_from_website else "",
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                float(data_from_website["longitude"]
                      ) if "longitude" in data_from_website else "",
                float(data_from_website["latitude"]
                      ) if "latitude" in data_from_website else "",
            ]
        }
    }


for city in cities:

    city = city[0]
    response = requests.get(city)

    # Cafes are stored in an array
    result = re.search(r"(?<=var cafes = )(.*?)(?=;)",
                       response.content.decode("utf-8"))

    if result:
        try:
            result_json = json.loads(result.group())
            download_data[city] = result_json
            for cafe in result_json:
                geojson["features"].append(get_feature(cafe))
        except:
            print(f"\n{city} failed")
            failed_cities.append(city)
    else:
        print(f"\n{city} has no cafe list")
        cities_without_list.append(city)

    print(".", end="")

print("\nDone downloading cities")

with open(f"coffee_{date.today()}.geojson", "w") as fp:
    json.dump(geojson, fp)

print("Failed:")
print(failed_cities)

print("No list:")
print(cities_without_list)
