# Specialty Coffee Neighborhoods

Required packages (install with pip):

- beautifulsoup4
- lxml
- requests-cache

## 1. Scrape European Coffee Trip

Run `ect_scrape.py`.

## 2. Get Neighborhood Information from OSM

Run `neighborhoods.py`.

## Resulting Files

- list of cafés: specialty_neighborhoods_date_.py
- list of amenities: specialty_neighborhoods_amenities_date_.py
- list of shops: specialty_neighborhoods_shops_date_.py

## Demo

Upload `kepler.gl.json` from this repo on https://kepler.gl/demo