import json
from geojson import FeatureCollection, Feature, Point
from turfpy.transformation import convex

geojson = {
    "type": "FeatureCollection",
    "features": []
}

geojson_out_cities = {
    "type": "FeatureCollection",
    "features": []
}

geojson_out_countries = {
    "type": "FeatureCollection",
    "features": []
}

with open("specialty_neighborhoods.geojson", "r") as fp:
    geojson = json.load(fp)


cafes_by_city = {}
cafes_by_country = {}

for feature in geojson["features"]:

    country = feature["properties"]["country"]
    city_ = feature["properties"]["city"]
    city = country + ", " + feature["properties"]["city"]

    if city not in cafes_by_city:
        cafes_by_city[city] = []

    if country not in cafes_by_country:
        cafes_by_country[country] = []

    cafes_by_city[city].append(feature)
    cafes_by_country[country].append(feature)


for city, cafes in cafes_by_city.items():

    fc_hull = []
    for cafe in cafes:
        print(cafe["geometry"]["coordinates"][:2])
        fc_hull.append(Feature(geometry=Point(cafe["geometry"]["coordinates"][:2])))

    fc_hull = FeatureCollection(fc_hull)

    hull = convex(fc_hull)

    hull["properties"]["name"] = city

    for cafe in cafes:
        for prop, val in cafe["properties"].items():
            if "count_" in prop:

                if prop not in hull["properties"]:
                    hull["properties"][prop] = int(val)
                else:
                    hull["properties"][prop] += int(val)

    geojson_out_cities["features"].append(hull)

with open("specialty_neighborhoods_cities.geojson", "w") as fp:
    json.dump(geojson_out_cities, fp, indent=4)


for country, cafes in cafes_by_country.items():

    fc_hull = []
    for cafe in cafes:
        print(cafe["geometry"]["coordinates"][:2])
        fc_hull.append(Feature(geometry=Point(cafe["geometry"]["coordinates"][:2])))

    fc_hull = FeatureCollection(fc_hull)

    hull = convex(fc_hull)

    coords = []

    for c in hull["geometry"]["coordinates"][0]:
        if 0.0 not in c:
            coords.append(c)

    hull["geometry"]["coordinates"] = [coords]

    hull["properties"]["name"] = country

    for cafe in cafes:
        for prop, val in cafe["properties"].items():
            if "count_" in prop:

                if prop not in hull["properties"]:
                    hull["properties"][prop] = int(val)
                else:
                    hull["properties"][prop] += int(val)

    geojson_out_countries["features"].append(hull)

with open("specialty_neighborhoods_countries.geojson", "w") as fp:
    json.dump(geojson_out_countries, fp, indent=4)
